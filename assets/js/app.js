import '../css/app.css';
import '../../node_modules/bulma/css/bulma.css'
let Vue = require('../../node_modules/vue/dist/vue');

let itemView = new Vue({
    el: '#site-body',
    data: {
        isActive: false,
        item: {
            ready: false,
            title: '',
            description: '',
            thumbnail: '',
            link: '',
            date: '',
            read: 0,
        },
    },
    delimiters: ['[{', '}]'],
    methods: {
        showItem: function (id) {
            let xhr = new XMLHttpRequest();
            let item = this.item;
            xhr.open("POST", "/item", true);
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        let jsonOutput = JSON.parse(xhr.responseText);
                        item.ready = true;
                        item.title = jsonOutput.title;
                        item.description = jsonOutput.description;
                        item.thumbnail = jsonOutput.thumbnail;
                        item.link = jsonOutput.link;
                        item.date = jsonOutput.date;
                        item.read = jsonOutput.read;
                        console.log(item.title);
                    } else {
                        console.error(xhr.statusText);
                    }
                }
            };
            xhr.onerror = function (e) {
                console.error(xhr.statusText);
            };
            xhr.send('{"id": "' + id + '"}');
            this.isActive = true;
        },
        close: function () {
            this.isActive = false;
            this.item = {
                ready: false,
                title: '',
                description: '',
                thumbnail: '',
                link: '',
                date: '',
                read: 0,
            };
        }
    }
})
itemView.isActive = false;

document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});