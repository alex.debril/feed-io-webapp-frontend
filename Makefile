build:
	docker-compose build --build-arg DOCKER_UID=$(shell id -u)
	docker-compose run fpm composer install

start:
	docker-compose up

stop:
	docker-compose down

shell:
	docker-compose run fpm bash

init-db:
	docker-compose run fpm ./bin/console app:init
