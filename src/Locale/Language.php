<?php declare(strict_types=1);


namespace App\Locale;


use Symfony\Component\HttpFoundation\Request;

class Language
{

    const localeMapping = [
        'en' => 'english',
        'fr' => 'french',
    ];

    public function getLang(Request $request): string
    {
        $locale = $request->getLocale();
        if (! isset(self::localeMapping[$locale])) {
            throw new \InvalidArgumentException("no language for $locale");
        }

        return self::localeMapping[$locale];
    }

}