<?php declare(strict_types=1);


namespace App\Data;


use FeedIo\Storage\Entity\Feed;
use FeedIo\Storage\Entity\Item;
use FeedIo\Storage\Repository\FeedRepository;
use FeedIo\Storage\Repository\ItemRepository;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\ObjectIdInterface;

class FeedProvider
{

    const topicsRedisKey = 'cache:topics-items:[topic-id]:[language]:[start]:[limit]';

    const feedRedisKey = 'cache:feed:[slug]';

    const feedItemsRedisKey = 'cache:feed-items:[feed-id]:[start]:[limit]';

    const cacheTtl = 60 * 3;

    private \Redis $redis;

    private FeedRepository $feedRepository;

    private ItemRepository $itemRepository;

    private TopicProvider $topicProvider;

    public function __construct(\Redis $redis, FeedRepository $feedRepository, ItemRepository $itemRepository, TopicProvider $topicProvider)
    {
        $this->redis = $redis;
        $this->feedRepository = $feedRepository;
        $this->itemRepository = $itemRepository;
        $this->topicProvider = $topicProvider;
    }

    public function getItemsFromTopic(ObjectIdInterface $topicId, string $language, int $start = 0, int $limit = 10)
    {
        $key = $this->getTopicCacheKey($topicId, $language, $start, $limit);
        $items = $this->redis->get($key);
        if ( ! $items ) {
            $iterator = $this->feedRepository->getItemsFromTopic($topicId, $language, $start, $limit);
            $items = [];
            foreach ($iterator as $item) {
                if ( ! isset($item['item']['thumbnail']) || is_null($item['item']['thumbnail'])) {
                    $topic = $this->topicProvider->getTopic($item['topicId']);
                    $item['item']['thumbnail'] = $topic->getImage();
                }
                $items[] = $item;
            }
            $this->redis->set($key, serialize($items), $this->getCacheTtl());
        } else {
            $items = unserialize($items);
        }
        return $items;
    }

    public function getFeed(string $slug): Feed
    {
        $key = $this->getFeedCacheKey($slug);
        $feed = $this->redis->get($key);
        if ( ! $feed ) {
            $feed = $this->feedRepository->getCollection()->findOne(
                ['slug' => $slug],
                ['typeMap' => ['root' => Feed::class]]
            );
            $this->redis->set($key, serialize($feed), $this->getCacheTtl());
        } else {
            $feed = unserialize($feed);
        }
        return $feed;
    }

    public function getItemsFromFeed(ObjectIdInterface $feedId, int $start = 0, int $limit = 10)
    {
        $key = $this->getFeedItemsCacheKey($feedId, $start, $limit);
        $items = $this->redis->get($key);
        if ( ! $items ) {
            $feed = $this->feedRepository->findOne($feedId);
            $topic = $this->topicProvider->getTopic($feed->getTopicId());
            $cursor = $this->itemRepository->getItemsFromFeed($feedId, $start, $limit);
            $items = [];
            /** @var Item $item */
            foreach($cursor as $item) {
                if (is_null($item->getThumbnail())) {
                    $item->setThumbnail($topic->getImage());
                }
                $items[] = $item;
            }
            $this->redis->set($key, serialize($items), $this->getCacheTtl());
        } else {
            $items = unserialize($items);
        }
        return $items;
    }

    private function getTopicCacheKey(ObjectIdInterface $topicId, string $language, int $start, int $limit)
    {
        return str_replace(
            ['[topic-id]', '[language]', '[start]', '[limit]'],
            [$topicId->__toString(), $language, $start, $limit],
            self::topicsRedisKey
        );
    }

    private function getFeedCacheKey(string $slug)
    {
        return str_replace(
            ['[slug]'],
            [$slug],
            self::feedRedisKey
        );
    }

    private function getFeedItemsCacheKey(ObjectIdInterface $feedId, int $start, int $limit)
    {
        return str_replace(
            ['[feed-id]', '[start]', '[limit]'],
            [$feedId->__toString(), $start, $limit],
            self::feedItemsRedisKey
        );
    }

    private function getCacheTtl(): int
    {
        return self::cacheTtl;
    }
}