<?php declare(strict_types=1);


namespace App\Data;


use FeedIo\Storage\Entity\Topic;
use FeedIo\Storage\Repository\TopicRepository;
use MongoDB\BSON\ObjectIdInterface;

class TopicProvider
{

    const homeTopics = ['news', 'tech'];

    const redisKey = 'cache:topics';

    const cacheTtl = 60 * 15;

    private \Redis $redis;

    private TopicRepository $topicRepository;

    public function __construct(\Redis $redis, TopicRepository $topicRepository)
    {
        $this->redis = $redis;
        $this->topicRepository = $topicRepository;
    }

    public function getTopic(ObjectIdInterface $id): Topic
    {
        $topics = $this->getTopics();
        /** @var Topic $topic */
        foreach ($topics as $topic) {
            if ($topic->getId() == $id) {
                return $topic;
            }
        }
        throw new \InvalidArgumentException("no topic found for id {$id->__toString()}");
    }

    public function getSlug(string $slug): Topic
    {
        $topics = $this->getTopics();
        foreach ($topics as $topic) {
            if ($topic->getSlug() == $slug) {
                return $topic;
            }
        }
        throw new \InvalidArgumentException("no topic found for slug {$slug}");
    }

    public function getHomeTopics(): array
    {
        $topics = $this->getTopics();
        $homeTopics = [];
        /** @var Topic $topic */
        foreach ($topics as $topic) {
            if (in_array($topic->getName()->getDefault(), self::homeTopics)) {
                $homeTopics[$topic->getName()->getDefault()] = $topic;
            }
        }
        return $homeTopics;
    }

    public function getTopics(): array
    {
        $topics = $this->redis->get(self::redisKey);
        if ( ! $topics ) {
            $cursor = $this->topicRepository->getTopics();
            $topics = [];
            foreach ($cursor as $topic) {
                $topics[] = $topic;
            }
            $this->redis->set(self::redisKey, serialize($topics), $this->getCacheTtl());
        } else {
            $topics = unserialize($topics);
        }
        return $topics;
    }

    private function getCacheTtl(): int
    {
        return self::cacheTtl;
    }

}