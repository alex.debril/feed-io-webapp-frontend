<?php declare(strict_types=1);


namespace App\Data;


use FeedIo\Storage\Entity\Feed;
use FeedIo\Storage\Entity\Item;
use FeedIo\Storage\Repository\FeedRepository;
use FeedIo\Storage\Repository\ItemRepository;
use MongoDB\BSON\ObjectIdInterface;

class ItemProvider
{
    const itemRedisKey = 'cache:item:[item-id]';

    const cacheTtl = 60 * 3;

    private \Redis $redis;

    private ItemRepository $itemRepository;

    public function __construct(\Redis $redis, ItemRepository $itemRepository)
    {
        $this->redis = $redis;
        $this->itemRepository = $itemRepository;
    }

    public function getItem(ObjectIdInterface $id): Item
    {
        $key = $this->getCacheKey($id);
        $item = $this->redis->get($key);
        if ( ! $item ) {
            $item = $this->itemRepository->findOne($id);
            if (is_null($item)) {
                throw new \UnexpectedValueException();
            }
            $this->redis->set($key, serialize($item), $this->getCacheTtl());
        } else {
            $item = unserialize($item);
        }
        return $item;
    }

    private function getCacheKey(ObjectIdInterface $id): string
    {
        return str_replace(
            ['[item-id]'],
            [$id->__toString()],
            self::itemRedisKey
        );
    }

    private function getCacheTtl(): int
    {
        return self::cacheTtl;
    }
}