<?php declare(strict_types=1);


namespace App\Command;


use FeedIo\Storage\Entity\Feed;
use FeedIo\Storage\Entity\Topic;
use FeedIo\Storage\Entity\Translations;
use FeedIo\Storage\Repository\FeedRepository;
use FeedIo\Storage\Repository\ItemRepository;
use FeedIo\Storage\Repository\TopicRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCommand extends Command
{

    private LoggerInterface $logger;

    private FeedRepository $feedRepository;

    private TopicRepository $topicRepository;

    public function __construct(
        LoggerInterface $logger,
        FeedRepository $feedRepository,
        TopicRepository $topicRepository
    )
    {
        $this->logger = $logger;
        $this->feedRepository = $feedRepository;
        $this->topicRepository = $topicRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:init')
            ->setDescription('init database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $topics = [];
        $output->writeln("creating topics");
        foreach ($this->getTopicsData() as $data) {
            $topic = new Topic();
            $topic->setSlug($data['default']);
            $topic->setName(new Translations($data['default'], ['english' => $data['en'], 'french' => $data['fr']]));
            $topics[$data['default']] = $this->topicRepository->save($topic)->getUpsertedId();
        }

        $output->writeln("creating feeds");
        foreach ($this->getFeedsData() as $data) {
            $feed = new Feed();
            $feed->setTopicId($topics[$data['topic']]);
            $feed->setLanguage($data['language']);
            $feed->setUrl($data['url']);
            $feed->setSlug(str_replace(['https://', 'http://', '/', '.'], ['', '', '-', '-'], $data['url']));
            $feed->setStatus(new Feed\Status(Feed\Status::ACCEPTED));
            $this->feedRepository->save($feed);
        }
        return 0;
    }

    protected function getFeedsData(): array
    {
        return [
            ['url' => 'http://php.net/feed.atom', 'topic' => 'tech', 'language' => 'english'],
            ['url' => 'http://feeds.bbci.co.uk/news/rss.xml', 'topic' => 'news', 'language' => 'english'],
            ['url' => 'https://www.lemonde.fr/rss/une.xml', 'topic' => 'news', 'language' => 'french'],
        ];
    }

    protected function getTopicsData(): array
    {
        return [
            ['default' => 'news', 'fr' => 'Actualités', 'en' => 'News'],
            ['default' => 'music', 'fr' => 'Musique', 'en' => 'Music'],
            ['default' => 'tech', 'fr' => 'Technologies', 'en' => 'Technology'],
            ['default' => 'science', 'fr' => 'Sciences', 'en' => 'Science'],
        ];
    }

}