<?php declare(strict_types=1);


namespace App\Controller;


use App\Data\FeedProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class FeedController extends AbstractController
{
    private FeedProvider $provider;

    public function __construct(FeedProvider $provider)
    {
        $this->provider = $provider;
    }

    public function index(string $slug): Response
    {
        $feed = $this->provider->getFeed($slug);
        return $this->render('feed.html.twig', [
            'feed' => $feed,
            'items' => $this->provider->getItemsFromFeed($feed->getId(), 0, 12),
        ]);
    }

}