<?php


namespace App\Controller;


use App\Data\FeedProvider;
use App\Data\TopicProvider;
use App\Locale\Language;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TopicController extends AbstractController
{
    private TopicProvider $topicProvider;

    private FeedProvider $feedProvider;

    private Language $lang;

    public function __construct(TopicProvider $topicProvider, FeedProvider $feedProvider, Language $lang)
    {
        $this->topicProvider = $topicProvider;
        $this->feedProvider = $feedProvider;
        $this->lang = $lang;
    }

    public function index(string $slug, Request $request): Response
    {
        $lang = $this->lang->getLang($request);
        try {
            $topic = $this->topicProvider->getSlug($slug);
            return $this->render('topic.html.twig', [
                'topic' => $topic->getName()->get($lang),
                'items' => $this->feedProvider->getItemsFromTopic($topic->getId(), $lang, 0, 12),
            ]);
        } catch (\InvalidArgumentException $e) {
            throw new NotFoundHttpException();
        }
    }
}