<?php declare(strict_types=1);


namespace App\Controller;

use App\Data\FeedProvider;
use App\Data\TopicProvider;
use App\Locale\Language;
use FeedIo\Storage\Entity\Topic;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{

    private TopicProvider $topicProvider;

    private FeedProvider $feedProvider;

    private Language $language;

    public function __construct(
        TopicProvider $topicProvider,
        FeedProvider $feedProvider,
        Language $language
    ){
        $this->topicProvider = $topicProvider;
        $this->feedProvider = $feedProvider;
        $this->language = $language;
    }

    public function index(Request $request): Response
    {
        $lang = $this->language->getLang($request);
        $topics = $this->topicProvider->getHomeTopics();
        $feeds = [];
        /** @var Topic $topic */
        foreach ($topics as $topic) {
            $feeds[$topic->getName()->getDefault()] = $this->feedProvider->getItemsFromTopic($topic->getId(), $lang, 0, 4);
        }

        return $this->render(
            'home.html.twig', [
                'topics' => $topics,
                'feeds' => $feeds,
        ]);
    }

}