<?php declare(strict_types=1);


namespace App\Controller;


use App\Data\ItemProvider;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\ObjectIdInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ItemController extends AbstractController
{
    const cacheKey = 'read:[id]';

    private ItemProvider $provider;

    private LoggerInterface $logger;

    private SessionInterface $session;

    private \Redis $redis;

    public function __construct(
        ItemProvider $provider,
        LoggerInterface $logger,
        SessionInterface $session,
        \Redis $redis
    ){
        $this->provider = $provider;
        $this->logger = $logger;
        $this->session = $session;
        $this->redis = $redis;
    }

    public function fetch(Request $request): JsonResponse
    {
        try {
            $payload = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
            if (!isset($payload->id)) {
                throw new \InvalidArgumentException("missing id in the payload");
            }
            $item = $this->provider->getItem(new ObjectId($payload->id));
            $this->markAsRead($item->getId());
            return new JsonResponse([
                '_id' => $item->getId(),
                'title' => $item->getTitle(),
                'thumbnail' => $item->getThumbnail(),
                'description' => $item->getDescription(),
                'date' => $item->getLastModified()->format('Y-m-d h:i'),
                'link' => $item->getLink(),
                'publicId' => $item->getPublicId(),
                'read' => $this->redis->get($this->getReadCacheKey($item->getId())),
            ]);
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400);
        } catch (\UnexpectedValueException $e) {
            return new JsonResponse(['error' => "not found"], 404);
        } catch (\Throwable $e) {
            $this->logger->error('failed to fetch item', [
                'error' => $e->getMessage(),
                'stack' => $e->getTraceAsString(),
            ]);
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

    }

    private function markAsRead(ObjectIdInterface $id)
    {
        $key = $this->getReadCacheKey($id);
        $read = $this->session->get($key);
        if (is_null($read)) {
            $this->redis->incr($key);
            $this->session->set($key, true);
        }
    }

    private function getReadCacheKey(ObjectIdInterface $id): string
    {
        return str_replace('[id]', $id->__toString(), self::cacheKey);
    }
}