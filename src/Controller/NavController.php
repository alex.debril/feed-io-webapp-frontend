<?php declare(strict_types=1);


namespace App\Controller;


use App\Data\TopicProvider;
use App\Locale\Language;
use FeedIo\Storage\Entity\Topic;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NavController extends AbstractController
{
    private TopicProvider $provider;

    private Language $language;

    const topTopics = ['news'];

    public function __construct(TopicProvider $provider, Language $language)
    {
        $this->provider = $provider;
        $this->language = $language;
    }

    public function nav(Request $request): Response
    {
        $providedTopics = $this->provider->getTopics();
        $lang = $this->language->getLang($request);

        $topics = [];
        /** @var Topic $topic */
        foreach ($providedTopics as $topic) {
            $offset = in_array($topic->getSlug(), self::topTopics) ? 'nav':'select';
            $topics[$offset][$topic->getSlug()] = $topic->getName()->get($lang);
        }
        return $this->render('nav.html.twig', [
            'topics' => $topics,
        ]);
    }


}